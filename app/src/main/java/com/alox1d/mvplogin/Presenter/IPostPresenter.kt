package com.alox1d.mvplogin.Presenter

import com.alox1d.mvplogin.Model.Post
import io.reactivex.Observable

interface IPostPresenter {
    fun initData()
    fun getObservables(): Observable<List<Post>>
}