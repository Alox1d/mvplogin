package com.alox1d.mvplogin.Presenter

import com.alox1d.mvplogin.Model.Post
import com.alox1d.mvplogin.Model.Retrofit.IPostAPI
import com.alox1d.mvplogin.Model.Retrofit.RetrofitClient
import com.alox1d.mvplogin.View.IPostView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class PostPresenter (var iPostView: IPostView) : IPostPresenter{

    override fun initData() {

        // fetchData
        val composite = CompositeDisposable()
        composite.add(getObservables().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{posts->iPostView.displayData(posts)}
        )

    }

    override fun getObservables():Observable<List<Post>> {
        val retrofit = RetrofitClient.instance
        val jsonApi = retrofit.create(IPostAPI::class.java)
        return jsonApi.posts
    }
}