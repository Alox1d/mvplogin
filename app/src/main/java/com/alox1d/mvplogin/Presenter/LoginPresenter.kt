package com.alox1d.mvplogin.Presenter

import com.alox1d.mvplogin.Model.User
import com.alox1d.mvplogin.View.ILoginView

class LoginPresenter (var iLoginView:ILoginView): ILoginPresenter {
    override fun onLogin(email: String, password: String) {
        val user = User(email,password)
        val loginCode = user.isDataValid()
        when(loginCode){
            0 -> iLoginView.onLoginError("Email must not be null")
            1 -> iLoginView.onLoginError("Wrong email address")
            2 -> iLoginView.onLoginError("Pass must be greater than 6")
            -1 -> iLoginView.onLoginSuccess("Login Success")
        }

    }


}