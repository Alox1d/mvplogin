package com.alox1d.mvplogin.Presenter

interface ILoginPresenter {
    fun onLogin(email:String, password:String)
}