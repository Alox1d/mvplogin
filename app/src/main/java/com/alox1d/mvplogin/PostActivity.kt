package com.alox1d.mvplogin

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alox1d.mvplogin.Model.Post
import com.alox1d.mvplogin.Presenter.PostPresenter
import com.alox1d.mvplogin.View.IPostView
import kotlinx.android.synthetic.main.activity_post.*
import kotlinx.android.synthetic.main.post_layout.view.*

class PostActivity : AppCompatActivity() , IPostView {


    lateinit var adapter:PostAdapter
    lateinit var postPresenter:PostPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)

        // view
        rv_posts.setHasFixedSize(true)
        rv_posts.layoutManager = LinearLayoutManager(this)
        adapter = PostAdapter(this)
        rv_posts.adapter = adapter
        postPresenter = PostPresenter(this)
        postPresenter.initData()


    }
    override fun displayData(postList: List<Post>) {
        for (post in postList) {
            adapter.add(post)
        }
        adapter.notifyDataSetChanged()
    }
    class PostAdapter (val context: Context) : RecyclerView.Adapter<PostViewHolder>(){
        private val postList:ArrayList<Post> = ArrayList()
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
            val itemView = LayoutInflater.from(context)
                .inflate(R.layout.post_layout, parent, false)
            return PostViewHolder(itemView)
        }

        override fun getItemCount(): Int {
            return postList.size
        }

        override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
            holder.tv_author.text = postList[position].userId.toString()
            holder.tv_content.text = StringBuilder(postList[position].body.substring(0,20)).append("...").toString()
            holder.tv_title.text = postList[position].title.toString()
        }
        fun add(post: Post) {
            postList.add(post)
        }
    }

    class PostViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val tv_author = itemView.tv_author
        val tv_title = itemView.tv_title
        val tv_content = itemView.tv_content
    }



}
