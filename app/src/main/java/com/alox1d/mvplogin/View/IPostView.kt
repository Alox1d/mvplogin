package com.alox1d.mvplogin.View

import com.alox1d.mvplogin.Model.Post

interface IPostView {
    fun displayData(postList:List<Post>)
}