package com.alox1d.mvplogin.View

interface ILoginView {
    fun onLoginSuccess(message:String)
    fun onLoginError(message: String)
}