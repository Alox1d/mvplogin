package com.alox1d.mvplogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.alox1d.mvplogin.Presenter.LoginPresenter
import com.alox1d.mvplogin.View.ILoginView
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_main.*

class LoginActivity : AppCompatActivity(), ILoginView {

    lateinit var loginPresenter:LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // init
        loginPresenter = LoginPresenter(this)

        //event
        btn_login.setOnClickListener{
            loginPresenter.onLogin(et_email.text.toString(), et_password.text.toString())
        }
    }


    override fun onLoginSuccess(message: String) {

        Toasty.success(this,message, Toast.LENGTH_SHORT).show()
        val intent: Intent = Intent(this, PostActivity::class.java)
        startActivity(intent)
        /* Display First Fragment initially */
//        val fragmentTransaction = fragmentManager.beginTransaction()
//        fragmentTransaction.replace(R.id.mainFragment, firstFragment)
//        fragmentTransaction.commit()
    }

    override fun onLoginError(message: String) {

        Toasty.error(this,message, Toast.LENGTH_SHORT).show()

    }
}
