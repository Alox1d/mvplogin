package com.alox1d.mvplogin.Model

interface IUser {
    val email:String
    val password:String
    fun isDataValid():Int
}