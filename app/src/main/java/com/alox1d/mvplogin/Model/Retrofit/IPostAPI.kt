package com.alox1d.mvplogin.Model.Retrofit

import com.alox1d.mvplogin.Model.Post
import io.reactivex.Observable
import retrofit2.http.GET

interface IPostAPI {
    @get:GET("posts")
    val posts: Observable<List<Post>>
}