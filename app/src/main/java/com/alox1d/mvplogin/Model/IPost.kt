package com.alox1d.mvplogin.Model

import io.reactivex.Observable
import java.util.*

interface IPost {
    val userId:Int
    val id:Int
    val title:String
    val body:String
    //fun loadPosts(): Observable<List<Post>>
}